export const environment = {
  production: true,
  firebase: {
    apiKey: "API_KEY",
    authDomain: "angular-todolist-64795.firebaseapp.com",
    databaseURL: "https://angular-todolist-64795.firebaseio.com",
    projectId: "angular-todolist-64795",
    storageBucket: "angular-todolist-64795.appspot.com",
    messagingSenderId: "727073141730",
    appId: "1:727073141730:web:a63a3ac6f68c6c65fbbfbb",
    measurementId: "G-V2ZQ8M9DCX"
  }
};
