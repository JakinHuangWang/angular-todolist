// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "API_KEY",
    authDomain: "angular-todolist-64795.firebaseapp.com",
    databaseURL: "https://angular-todolist-64795.firebaseio.com",
    projectId: "angular-todolist-64795",
    storageBucket: "angular-todolist-64795.appspot.com",
    messagingSenderId: "727073141730",
    appId: "1:727073141730:web:a63a3ac6f68c6c65fbbfbb",
    measurementId: "G-V2ZQ8M9DCX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
